<!--
 * @Author: WangZhongDe
 * @Date: 2021-07-24 17:16:40
 * @LastEditors: WangZhongDe
 * @LastEditTime: 2021-08-19 09:01:10
 * @FilePath: \gitLearn\Readme.md
-->
# git学习笔记
## 安装git
### Linux 上安装
* 先检查有没有安装git
```shell
$ git
The program 'git' is currently not installed. You can install it by typing:
sudo apt-get install git
``` 
  像上面的命令，有很多Linux会友好地告诉你Git没有安装，还会告诉你如何安装Git。
* Debian或Ubuntu Linux 安装命令如下：  
```
sudo apt-get install git
```
* 如果是其他Linux版本，可以直接通过源码安装。先从Git官网下载源码，然后解压，依次输入：./config，make，sudo make install这几个命令安装就好了
### 在Windows上安装Git
* 下载地址：https://git-scm.com/downloads
* 安装完成后，在开始菜单里找到“Git”->“Git Bash”，蹦出一个类似命令行窗口的东西，就说明Git安装成功！
* 安装完成后，还需要最后一步设置，在命令行输入：
```
$ git config --global user.name "Your Name"
$ git config --global user.email "email@example.com"
``` 
## 创建版本库
* 一、新建一个项目文件夹
* 二、通过git init 命令可以把这个文件夹目录变成git可以管理的仓库
* 打开命令行工具,并将路径切换到这个目录里面,执行以下命令：
```
$ git init 
```
## 添加文件到版本库
* 一、git add 文件名  
  二、git commit 文件名 -m"说明信息"
```
git add Readme.md
git commit Readme.md -m"git install"
```
## 查看当前仓库状态
```
$ git status
```
## 查看具体修改内容
```
$ git diff readme.txt 
```
### 查看提交日志
```
$ git log --pretty=oneline
```
### 退回版本
* 在Git中，用HEAD表示当前版本,上一个版本就是HEAD^，上上一个版本就是HEAD^^,往上100个版本写成HEAD~100
* 回退版本命令是 git reset,例子：退回到上一个版本
```
$ git reset --hard HEAD^
```
* 退回到指定的版本
```
$ git reset --hard 1094a //1094a是commit_id 可以通过git log 查询commit_id
```
* 用git reflog查看命令历史
* 用git log可以查看提交历史
* HEAD指向的版本就是当前版本
### 工作区（Working Directory）
* 就是你在电脑里能看到的目录，比如我的gitLearn文件夹就是一个工作区 
### 版本库（Repository）
* 工作区有一个隐藏目录.git，这个不算工作区，而是Git的版本库。
### 撤销修改 (丢弃工作区的修改)
```
$ git checkout -- Readme.md 
```
### 删除文件
```
$ git rm test.txt
```
## 远程仓库
### 第1步：创建SSH Key
* 在用户主目录下，看看有没有.ssh目录，如果有，再看看这个目录下有没有id_rsa和id_rsa.pub这两个文件，如果已经有了，可直接跳到下一步。如果没有，打开Shell（Windows下打开Git Bash），创建SSH Key：
```
$ ssh-keygen -t rsa -C "wangzd369@qq.com" 
```
* 用户主目录,如windows系统中是：C:\Users\Administrator
### 第2步：登陆gitee 打开修改资料页面下的“SSH公钥”页面：然后，点“添加SSH公钥”，填上任意标题，在公钥Key文本框里粘贴id_rsa.pub文件的内容后保存  
### 第3步：登录gitee 新建仓库
### 第4步：关联本地仓库
```
$ git remote add gitLearn git@gitee.com:wangzd9988/gitLearn.git
```
### 第5步：把本地仓库推到远程仓库
```
$ git push -u gitLearn master
```
* 把本地库的内容推送到远程，用git push命令，实际上是把当前分支master推送到远程
* 由于远程库是空的，我们第一次推送master分支时，加上了-u参数，Git不但会把本地的master分支内容推送的远程新的master分支，还会把本地的master分支和远程的master分支关联起来，在以后的推送或者拉取时就可以简化命令。
* 以后只要本地作了提交，就可以通过命令：
```
  $ git push gitLearn master
```
### 删除远程库(解除本地与远程库的关联)
* 可以用git remote rm <name>命令。使用前，建议先用git remote -v查看远程库信息：
```
$ git remote -v
```
* 然后，根据名字删除，比如删除gitLearn： 
```
$ git remote rm gitLearn
```
### 从远程库克隆
* 使用命令git clone 从远程库克隆到本地,例子:
```
$ git clone git@gitee.com:wangze9988/gitLearn.git 
```
## 分支管理
### 创建分支
```
$ git branch dev
```
### 切换分支
```
$ git checkout dev 
```
### 切换分支(新命令)
```
$ git switch master
```
### 创建并切换到当前分支
```
$ git checkout -b dev
```
### 创建并切换到当前分支（新命令）
```
$ git switch -c dev
```
### 用git branch命令查看当前分支
```
$ git branch
```
### git merge命令用于合并指定分支到当前分支
```
$ git merge dev  
```
### 删除分支dev
```
$ git branch -d dev
```
### 分支管理策略
* 通常，合并分支时，如果可能，Git会用Fast forward模式,但这种模式下,删除分支后,会丢掉分支信息。
* 如果要强制禁用Fast forward模式，Git就会在merge时生成一个新的commit,这样,从分支历史上就可以看出分支信息。
* 合并dev分支，请注意--no-ff参数，表示禁用Fast forward：
```
$ git merge --no-ff -m "merge with no-ff" dev
```
### 查看分支历史
```
$ git log 
```
* Git还提供了一个stash功能，可以把当前工作现场“储藏”起来，等以后恢复现场后继续工作：
```
$ git stash
```
* 用git stash list命令查看之前暂存的工作分支
```
$ git stash list
```
* git stash apply命令恢复，但是恢复后，stash内容并不删除
* 需要用git stash drop来删除
* 用git stash pop，恢复的同时把stash内容也删了
```
$ git stash pop
```
* 可以多次stash，恢复的时候，先用git stash list查看，然后恢复指定的stash，用命令：
``` 
$ git stash apply stash@{0}
```
* Git专门提供了一个cherry-pick命令，让我们能复制一个特定的提交到当前分支：  
```
$ git cherry-pick 4c805e2
```
### 强行删除分支
* 如果要丢弃一个没有被合并过的分支，可以通过git branch -D <name>强行删除。
```
$ git branch -D feature-vulcan
```
### 查看远程库信息
* 查看远程库的信息，用git remote,加-v参数显示详细信息
```
$ git remote -v
```
### 推送分支
* 推送分支，就是把该分支上的所有本地提交推送到远程库。推送时，要指定本地分支，这样，Git就会把该分支推送到远程库对应的远程分支上：
```
$ git push origin master
```
## 多人协作
* 查看远程库信息，使用git remote -v；
* 本地新建的分支如果不推送到远程，对其他人就是不可见的；
* 从本地推送分支，使用git push origin branch-name，如果推送失败，先用git pull抓取远程的新提交；
* 在本地创建和远程分支对应的分支，使用git checkout -b branch-name origin/branch-name，本地和远程分支的名称最好一致；
* 建立本地分支和远程分支的关联，使用git branch --set-upstream branch-name origin/branch-name；
* 从远程抓取分支，使用git pull，如果有冲突，要先处理冲突。
## Rebase
* rebase操作可以把本地未push的分叉提交历史整理成直线；
* rebase的目的是使得我们在查看历史提交的变化时更容易，因为分叉的提交需要三方对比。  
## 标签管理   
### 创建标签
* 首先，切换到需要打标签的分支上执行以下命令：
  ```
  $ git tag v1.0
  ```
### 查看所有标签：
```
$ git tag
```
### 对指定的commit_id(f52c633)打标签
```
$ git tag v0.9 f52c633
```
### 注意，标签不是按时间顺序列出，而是按字母排序的。可以用git show <tagname>查看标签信息：
```
$ git show v0.9
```
### 创建带有说明的标签，用-a指定标签名，-m指定说明文字：
```
$ git tag -a v0.1 -m "version 0.1 released" 1094adb
```
### 删除标签
```
$ git tag -d v0.1
```
### 推送某个标签到远程，使用命令git push origin <tagname>：
```
$ git push origin v1.0
```
### 一次性推送全部尚未推送到远程的本地标签：
```
$ git push origin --tags
```
### 命令git push origin :refs/tags/<tagname>可以删除一个远程标签。

## 自定义Git
### 让Git显示颜色，会让命令输出看起来更醒目：
```
$ git config --global color.ui true
```
### 忽略特殊文件
* 忽略某些文件时，需要在工作目录下编写名为.gitignore的文件
* 文件内空填写要忽略的文件名如:
 ```
 .gitignore
Thumbs.db
ehthumbs.db
Desktop.ini
readme.txt
*.txt
```
* .gitignore文件本身要放到版本库里，并且可以对.gitignore做版本管理！ 
* .gitignore只能忽略那些原来没有被track的文件，如果某些文件已经被纳入了版本管理中，则修改.gitignore是无效的。
* 想要.gitignore起作用，必须要在这些文件不在暂存区中才可以，.gitignore文件只是忽略没有被staged(cached)文件,对于已经被staged文件，加入ignore文件时一定要先从staged移除，才可以忽略。
* 还可以先备份本地文件,然后再git rm 删除文件,再还原文件,可以解决.gitignore不生效的问题
 ### 设置不要检查特定文件的更改情况(忽略特殊文件方法二：)
 ```
 git update-index --assume-unchanged PATH 
 ```
 * 上例命令中的PATH是要忽略的文件名
### git清除本地缓存
```
git rm -r --cached .
```
### 删除远程仓库中的文件但保留本地文件
* 1、删除缓存
```
git rm -r --cached readme.txt
```
* 2、commit 
* 3、push
### 配置别名

 * 配置st 代替status命令 
```
git config --global alias.st status
``` 
* 执行以上命令后,现在敲git st与git status就是一样的效果了
### 配置文件
* 配置文件的路径：
  - 每个仓库的Git配置文件都放在.git/config文件中   
## 搭建Git服务器
* 搭建Git服务器需要准备一台运行Linux的机器，强烈推荐用Ubuntu或Debian，这样，通过几条简单的apt命令就可以完成安装。假设你已经有sudo权限的用户账号，下面，正式开始安装。   
* 第一步，安装git：
```
$ sudo apt-get install git
```
* 第二步，创建一个git用户，用来运行git服务：
```
$ sudo adduser git
```
* 第三步，创建证书登录：
  - 收集所有需要登录的用户的公钥，就是他们自己的id_rsa.pub文件，把所有公钥导入到/home/git/.ssh/authorized_keys文件里，一行一个。
* 第四步，初始化Git仓库：
  - 先选定一个目录作为Git仓库，假定是/srv/sample.git，在/srv目录下输入命令：
    ```
    $ sudo git init --bare sample.git
    ```
  - Git就会创建一个裸仓库，裸仓库没有工作区，因为服务器上的Git仓库纯粹是为了共享，所以不让用户直接登录到服务器上去改工作区，并且服务器上的Git仓库通常都以.git结尾。然后，把owner改为git：  
    ```    
    $ sudo chown -R git:git sample.git
    ```
* 第五步，禁用shell登录：
  - 出于安全考虑，第二步创建的git用户不允许登录shell，这可以通过编辑/etc/passwd文件完成。找到类似下面的一行：
  ```
  git:x:1001:1001:,,,:/home/git:/bin/bash
  ```
  - 改为：
  ```
  git:x:1001:1001:,,,:/home/git:/usr/bin/git-shell
  ```
  - 这样，git用户可以正常通过ssh使用git，但无法登录shell，因为我们为git用户指定的git-shell每次一登录就自动退出。
* 第六步，克隆远程仓库：
  - 现在，可以通过git clone命令克隆远程仓库了，在各自的电脑上运行： 
  ```
  git clone git@server:/srv/sample.git
  ```
  ### 管理公钥
  * 如果团队很小，把每个人的公钥收集起来放到服务器的/home/git/.ssh/authorized_keys文件里就是可行的。如果团队有几百号人，就没法这么玩了，这时，可以用Gitosis来管理公钥。 
### 管理权限
* 有很多不但视源代码如生命，而且视员工为窃贼的公司，会在版本控制系统里设置一套完善的权限控制，每个人是否有读写权限会精确到每个分支甚至每个目录下。因为Git是为Linux源代码托管而开发的，所以Git也继承了开源社区的精神，不支持权限控制。不过，因为Git支持钩子（hook），所以，可以在服务器端编写一系列脚本来控制提交等操作，达到权限控制的目的。Gitolite就是这个工具。
## git图形管理工具
* Git有很多图形界面工具，这里我们推荐SourceTree，它是由Atlassian开发的免费Git图形界面工具，可以操作任何Git库。
* 下载地址：https://www.sourcetreeapp.com/ 
* 

##   免输入密码操作远程仓库
* 执行远程仓库操作需要输入密码是件比较麻烦的事情，在配置文件的url里配上用户名和密码即可免掉这样的麻烦，提高操作效率。免输密码操作远程仓库还可以通过ssh方式实现，下面只给出https方式的免输密码配置： 
  ```
  url = https://${user}:${password}@github.com/Bwar/Nebula.git

  ``` 
* 把上面配置中的“${user}”和“${password}”用你的远程仓库用户名和密码代入即可。  
